<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers;

/**
 * Description of MagazineController
 *
 * @author Rick
 */
class MagazineController extends Controller {
    
    
    
    public function show() {
        $magazinesCollection = collect(\Illuminate\Support\Facades\DB::select('
            select * 
            from magazine m 
            join text t 
            on m.textId = m.id
            join publication p
            on t.publicationId = p.id'
        ));
        
        $textIds = $magazinesCollection->pluck('textId');
        
        $authorsCollection = \Illuminate\Support\Facades\DB::table('author')
            ->join('text_author', 'author.id', '=', 'text_author.authorId')
            ->whereIn('text_author.textId', $textIds)
            ->get();
        
        foreach($magazinesCollection as $magazine)
        {
            $magazine->authors = $authorsCollection->where('textId', $magazine->textId)->all();
        }
        
        return $magazinesCollection;
    }
}
